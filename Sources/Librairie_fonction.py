#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 10 14:39:23 2018

@author: Cassandre et Sarah
"""

import networkx as nx
import matplotlib.pyplot as plt
import heapq
import random
import math
import time
import os

# Fonction de lecture de fichier 

def parse_instance(file_path):
    """ parse_instance prend en paramètre un nom de fichier et retourne le graphe de son contenue"""
    # Instanciation d'un graph vide :
    G = nx.Graph()

    # Recuperation des Sommets Terminaux :
    fichier = os.popen("grep '^T \\d*' {} | cut -d \" \" -f2 ".format(file_path), "r")
    lect_v = fichier.readlines()
    # Retirer les "\n" :
    lect_v = "".join(lect_v)
    lect_v = lect_v.split("\n")
    lect_v.remove("")
    #print(lect_v )
    fichier.close()
    
    # Recuperation des Arêtes :
    fichier = os.popen("grep '^E \\d* \\d* \\d*' {} | cut -d \" \" -f2,3,4 ".format(file_path), "r") 
    #fichier = os.popen("grep '^E [1-9]* [a-z1-9]* [a-z1-9]*' {} | cut -d \" \" -f2,3,4 ".format(file_path), "r") 
    
    lect = fichier.readlines()
    # Retirer les "\n" :
    lect = "".join(lect)
    lect = lect.split("\n")
    lect.remove("")
    #print(lect )
    fichier.close()
    
    # Sommets terminaux :
    sommets_int = []    
    sommets_str = []
    for v in lect_v :
        sommets_str .append('v'+v)
        sommets_int.append(int(v))
    G.add_nodes_from(sommets_str , Terminal = True)
    
    # Creation du dictionnaire des arêtes :
    for edge in lect :
        # Arête à coordonner string :
        edge_str = edge.split(" ")
        # Arête à coordonner entière : 
        edge_int = (int(edge_str[0]), int(edge_str[1]), int(edge_str[2]))
        # Insertion d'arête dans la le graph + Insert de sommet non terminaux dans la liste de sommet:
        if edge_int[0] in sommets_int and edge_int[1] in sommets_int :
            G.add_edge('v'+edge_str[0],'v'+edge_str[1],weight=int(edge_str[2]))

        elif edge_int[0] not in sommets_int and edge_int[1] in sommets_int :
            G.add_edge('u'+edge_str[0],'v'+edge_str[1],weight=int(edge_str[2]))
            G.add_node('u'+edge_str[0], Internal=True)
        elif edge_int[0] in sommets_int and edge_int[1] not in sommets_int :
            G.add_edge('v'+edge_str[0],'u'+edge_str[1], weight=int(edge_str[2]))
            G.add_node('u'+edge_str[1], Internal=True)
        else :
            G.add_edge('u'+edge_str[0],'u'+edge_str[1], weight=int(edge_str[2]))
            G.add_node('u'+edge_str[0], Internal=True)
            G.add_node('u'+edge_str[1], Internal=True)
    return G

def affichage (G) :
    """ Affichage de manière graphique du graphe passé en paramètre """
    for v in G.nodes():
        G.node[v]['state']= v
    pos = nx.spring_layout(G)
    nx.draw(G, pos)
    node_labels = nx.get_node_attributes(G,'state')
    nx.draw_networkx_labels(G, pos, labels = node_labels)
    edge_labels = nx.get_edge_attributes(G,'state')
    nx.draw_networkx_edge_labels(G, pos, labels = edge_labels)
    #plt.savefig('this.png')
    print("\n")
    plt.show()
 
def calcul_fitness_arbre(G) :
    """" Retourne la somme des arêtes du graphe G """
    fitness = 0
    for node1, node2 in G.edges() :
        fitness += G[node1][node2]['weight']
    return fitness    

""" HEURISTIQUE DE CONSTRUCTION """

def heuristique_plus_court_chemin(G) :
    # Step 1 : complet graph (G1)
    G1 = nx.Graph()
    # Instancie les sommets terminaux :
    G1.add_nodes_from(list(nx.get_node_attributes(G,"Terminal")))
    # Calcul le plus courts chemin du graph complet par Dijkstra :
    dico_dijkstra = nx.all_pairs_dijkstra_path_length(G)
    # Construction du graphe :
    for node1 in G1.nodes() :
        for node2 in G1.nodes()  :
            if node1 != node2 :
                G1.add_edge(node1, node2, weight=dico_dijkstra[node1][node2])
    # Step 2 : arbre de poids minimum (G2)
    G2 = nx.minimum_spanning_tree(G1)
    # Step 3 : sous graphe G3 de G (G3)
    G3 = nx.Graph()
    list_node = set()
    # copy de G + suppression de l'arête G2 si elle existe !!
    for node1, node2 in G2.edges() :
        short_path = nx.dijkstra_path(G,node1,node2)
        # Insertion des arêtes avec le poids dans le graph + node:
        for index in range(len(short_path)-1) :
            node_1, node_2 = short_path[index], short_path[index+1]
            list_node.add(node_1)
            list_node.add(node_2)
            G3.add_edge(node_1, node_2, weight=G[node_1][node_2]['weight'])
    # Insertion des nodes dans G3 avec data :
    for node in list_node :
        if node[0] == 'v' :
            G3.add_node(node, Terminal = True)
        else :
            G3.add_node(node, Internal = True)
    G3.add_nodes_from(list_node)
    # Step 4 : arbre de poids minimum de G3 (G4) :
    G4 = nx.minimum_spanning_tree(G3)  
    # Step 5 : élimination les feuille non-terminal :
    dico_degree_internal = G4.degree(list(nx.get_node_attributes(G,"Internal")))
    for node, value in dico_degree_internal.items() :
        if value == 1 and G4.node[node]=={'Internal':True}:
            G4.remove_node(node)
    return G4

def test_heuristique_plus_court_chemin() : 
    for i in range(1,21) :
        if i < 10 :
            G = parse_instance("D/d0"+str(i)+".stp")
        else :
            G = parse_instance("D/d"+str(i)+".stp")
        start = time.time()
        resultat = heuristique_plus_court_chemin(G)
        end = time.time()
        print("D/d0"+str(i)+".stp : " + str(calcul_fitness_arbre(resultat)) + " time : " + str(end-start))
    


def heuristique_arbre_couvrant_minimum(G) :
    # Construire arbre couvrant du graphe original G :
    G_new = nx.minimum_spanning_tree(G)
    while True :
        # Teste si toutes les feuilles de l'arbre sont des sommets terminaux :
        dico_degree = G_new.degree(G_new.nodes())
        end_possible = True
        for node, value in dico_degree.items() :
            if value == 1 and G_new.node[node]=={'Internal':True}:
                end_possible = False
                break
            
        if end_possible == True :
            break

        # Suppression des sommets non-terminaux de degré 1 :
        for node in list(nx.get_node_attributes(G_new,"Internal")) :
            if dico_degree[node] == 1 :
                G_new.remove_node(node) # Regarder qu'on ne supprime pas de sommet terminal !
    return G_new

def test_heuristique_arbre_couvrant_minimum() : 
    for i in range(1,21) :
        if i < 10 :
            G = parse_instance("E/e0"+str(i)+".stp")
        else :
            G = parse_instance("E/e"+str(i)+".stp")
        start = time.time()
        resultat = heuristique_arbre_couvrant_minimum(G)
        end = time.time()
        print("E/e0"+str(i)+".stp : " + str(calcul_fitness_arbre(resultat)) + " time : " + str(end-start))


   

""" ALGORITHME GENETIQUE """


# INITIALISATION POPULATION :
def initialisationPopulationRandom(population, taille_population, taille_codage, p) :
    """ Retourne une population d'individu initialisé de façon aléatoire """
    for i in range(taille_population) :
        if random.random() < p :
            new_individu = [1 for bit_index in range(taille_codage)]
        else :
            new_individu = [0 for bit_index in range(taille_codage)] 
        population.append(new_individu)
    return population

def initialisationPopulationHeuristique(G, population, taille_population, dico_reinternals, taille_codage) :
    """ Retourne une population d'individu initialisé par randomisation des heuristiques de construction """
    # Algorithme de randomisation :
    for i in range(taille_population) :
        if i == 0 :
            # Heuristique 1 :
            graphe_heuristique1 = heuristique_plus_court_chemin(G)
            new_individu = [0]*taille_codage
            for sommet_i in list(nx.get_node_attributes(graphe_heuristique1,"Internal")) :
                new_individu[dico_reinternals[sommet_i]] = 1
        elif i == 1 : 
            # Heuristique 2 :
            graphe_heuristique2 = heuristique_arbre_couvrant_minimum(G)
            new_individu = [0]*taille_codage
            for sommet_i in list(nx.get_node_attributes(graphe_heuristique2,"Internal")) :
                new_individu[dico_reinternals[sommet_i]] = 1
        else :
            # Randomisation des heuristiques :
            choix_heuristique = random.randint(0,1)
            graphe = G.copy()
            # Alteration des poids des arêtes de 5 à 10% :
            for s1, s2, cout in graphe.edges(data='weight'): 
                value_alea = random.randint(5,20)
                new_cout = int(cout*(value_alea/100))
                alea_sup_inf = random.randint(0,1)
                if alea_sup_inf == 0 :
                    graphe[s1][s2]['weight'] = cout + new_cout
                else :
                    if cout - new_cout < 0 :
                        graphe[s1][s2]['weight'] = 0
                    else :
                        graphe[s1][s2]['weight'] = cout - new_cout
            if choix_heuristique == 0 :
                graphe = heuristique_plus_court_chemin(graphe)
            else :
                graphe = heuristique_arbre_couvrant_minimum(graphe)
            new_individu = [0]*taille_codage
            for sommet_i in  list(nx.get_node_attributes(graphe,"Internal")) :
                new_individu[dico_reinternals[sommet_i]] = 1
        population.append(new_individu)
    return population

# FITNESS :
def codage_individu(G) :
    """ Retourne le codage d'un individu """
    # Dictionnaire de sommet internal pour recuperer leur position dans le codage :
    dico_internals = dict()
    dico_reinternals = dict()
    # Recupere les noeuds internals :
    nodes_internals = list(nx.get_node_attributes(G,"Internal"))
    # Recupere les noeuds terminals :
    nodes_terminals = list(nx.get_node_attributes(G,"Terminal"))
    # Instanciation du dictionnaire :
    for i, node in enumerate(nodes_internals) :
       dico_internals[i] = node
       dico_reinternals[node] = i
    return nodes_internals, nodes_terminals, dico_internals, dico_reinternals

def fitness_individu(G, nodes_terminals, dico_internals, codage, M) :
    # Transformation du codage en une liste de sommets :
    nodes_internals_select = [dico_internals[i] for i,bit in enumerate(codage) if bit == 1]
    # Sous graphe induit par les sommets terminaux et non-terminaux :
    G_induit = nx.subgraph(G,nodes_terminals + nodes_internals_select)
    # Arbre couvrant :
    G_kruskal = nx.minimum_spanning_tree(G_induit)
    # Calcul de la fitness :
    if nx.is_connected(G_kruskal) :
        return calcul_fitness_arbre(G_kruskal)
    else :
        return calcul_fitness_arbre(G_kruskal) + M * (nx.number_of_nodes(G_kruskal) - 1 - nx.number_of_edges(G_kruskal))
    
def fitness_population(G, nodes_terminals, dico_internals, population, M) :
    # Declaration de la liste des fitness d'une population :
    list_fitness = list()
    # Instanciation de la liste fitness:
    for individu in population :
        list_fitness.append(fitness_individu(G, nodes_terminals, dico_internals, individu, M))
    return list_fitness

# SELECTION :
    
def selections_meilleurs_fitness(population, fitness, num) :
    """ Selectionne les individus ayant les meilleurs paramètres """
    list_population, select_population, select_fitness = list(), list(), list()
    for fit, individu in zip(fitness, population) :
        heapq.heappush(list_population, (fit, individu))
    for i in range(num) :
        elem = heapq.heappop(list_population)
        select_fitness.append(elem[0])
        select_population.append(elem[1])
    return select_population, select_fitness
        
def selections_roulettes(population, fitness, num):
     """ Selection roulette avec un  num =  Taille population / 2 """
     # Max des fonctions fitness :
     max_f = max(fitness)
     # Nouvelle liste fiteness :
     f_prime = [max_f - f for f in fitness]
     # Total des fitness de la popluation :
     total_fitness = float(sum(f_prime))
     # Pourcentage fitness pour chaque individu :
     rel_fitness = [(f/total_fitness) for f in f_prime]
     # Genere une intervalle de probabilite pour chaque individu :
     probs = [sum(rel_fitness[:i+1]) for i in range(len(rel_fitness))]
     # Selection des individus :
     new_population = []
     fit = []
     for n in range(num):
         present = True
         while present :
             r = random.random()
             for (i, individual) in enumerate(population):
                 if r <= probs[i]:
                     if individual in new_population :
                         present = True
                         continue
                     else :
                         present = False
                         new_population.append(individual)
                         fit.append(fitness[i])
                         break 
     return new_population, fit  

# CROISEMENT :

def croisement_one_point (population, n, taille) :
    """ Croisement en 1 point sur n individus : taille individu """
    pop = population.copy()
    res = []
    for i in range(n) :
        parent1 = pop[random.randint(0, len(pop)-1)]
        pop.remove(parent1)
        parent2 = pop[random.randint(0, len(pop)-1)]
        pop.remove(parent2)
        point = random.randint(1, taille-1)
        enfant1 = parent1[:point] + parent2[point:]
        enfant2 = parent2[:point] + parent1[point:]
        res.append(enfant1)
        res.append(enfant2)
    return res 

# MUTATION :
    
def mutation_population(population, pourcentage_mutation, taille):
    """ Retourne une population mutée """
    mutation_pop = []
    for pop in population :
        if random.random() < pourcentage_mutation :
            pop[random.randint(0,taille-1)] = random.randint(0,1)
        mutation_pop.append(pop)
    return mutation_pop

# REMPLACEMENT :
    
def fonction_genetique (G, taille_population, pourcentage_mutation, p, nb_iteration, M, choix_initialisation, choix_remplacement) :
    # DONNEES :
    population = []
    iterator = 0

    bestFitness = math.inf # pire evaluation
    bestCodage = [] 
    
    nodes_internals, nodes_terminals, dico_internals, dico_reinternals = codage_individu(G)
    taille_codage = len(nodes_internals)

    start = time.time()
    while iterator < nb_iteration:
        if iterator ==  0 :
            if choix_initialisation == 0 :
                population = initialisationPopulationRandom(population, taille_population, taille_codage, p)
            else :
                population = initialisationPopulationHeuristique(G, population, taille_population, dico_reinternals, taille_codage)
        else :
            # Recuperer tout les individus avec leurs fitness :    
            liste_fitness = fitness_population(G, nodes_terminals, dico_internals, population, M)
            
            # Selectionner les meilleurs fitness de la population :
            evaluate = population[liste_fitness.index(min(liste_fitness))], min(liste_fitness)
            
            # Affiche Aide :    
            if bestFitness > evaluate[1] :
                bestFitness = evaluate[1]
                bestCodage = evaluate[0].copy()
            
            # Selection des individus :
            #parent_selectionnee = selections_roulettes(population, liste_fitness, taille_population//2)
            if choix_remplacement == 0 :
                # Creation d'une population par remplacement élitiste :
                parent_selectionnee = selections_meilleurs_fitness(population, liste_fitness, taille_population//2)
                enfants = croisement_one_point(parent_selectionnee[0], taille_population//4, taille_codage)
                new_population = parent_selectionnee[0] + enfants
            else :
                # Creation d'une population par remplacement générationnel :
                parent_selectionnee = selections_meilleurs_fitness(population, liste_fitness, taille_population)
                enfants = croisement_one_point(parent_selectionnee[0], taille_population//2, taille_codage)
                new_population = enfants
                            
            # Operateur de mutation sur l'ensemble de la population :
            population = mutation_population(new_population, pourcentage_mutation, taille_codage)          
        iterator = iterator + 1

    #Affichage des meilleurs fitness :
    print ("\nFitness: ", bestFitness)
    print ("BestCodage: ", bestCodage,"\n")
    end = time.time()
    
    return (bestFitness, end-start)#, bestCodage, dico_internals, end-start


def test_genetique_parametre(G, l_taille_population, l_pourcentage_mutation, l_nb_iteration, l_p, l_choix_remplacement) :
    """ Pour ce donner une idée des meilleurs paramètres à choisir """
    list_best_params = list()
    
    for pop in l_taille_population :
        for mut in l_pourcentage_mutation :
            for ite in l_nb_iteration :
                for remp in l_choix_remplacement :
                    for prob in l_p :
                        list_best_params.append((fonction_genetique (G, pop, mut, prob, ite, 100, 0, remp), (pop, mut, ite, remp, prob)))
                        
    print(min(list_best_params))

    
def test_genetique_1(G,taille_population, pourcentage_mutation, p, nb_iteration, M, choix_remplacement) :
    for i in range(16,21) :
        if i < 10 :
            G = parse_instance("E/e0"+str(i)+".stp")
        else :
            G = parse_instance("E/e"+str(i)+".stp")
        resultat, time = fonction_genetique (G, taille_population, pourcentage_mutation, p, nb_iteration, M, 1, choix_remplacement)
        print("E/e0"+str(i)+".stp : " + str(resultat) + " time : " + str(time))
    

""" RECHERCHE LOCALE """

def recherche_locale(G, choix_heuristique) :    
    # Solution initiale :
    if choix_heuristique == 0 :
        G_init = heuristique_plus_court_chemin(G) 
    else :
        G_init = heuristique_arbre_couvrant_minimum(G) 
    
    # Solution init :
    fitness_solution = calcul_fitness_arbre(G_init)
    
    # Dictionnaire de visite des noeuds non-terminaux :
    dico_internals = dict()
    for node in list(nx.get_node_attributes(G,"Internal")) :
        dico_internals[node] = False
        
    while True :
        # Fonction de voisinage :
        fit = math.inf
        while True :
            nodes_S = list(nx.get_node_attributes(G_init,"Internal"))
            nodes_internals_no_S = [node for node in list(nx.get_node_attributes(G,"Internal")) if node not in nodes_S]
            # Boucle pour calculer les solutions avec l'ajout d'un sommet non-terminaux ne faisant pas parti de l'ensemble S:
            voisin = list()  # liste de solution
            for candidat_s in nodes_internals_no_S :
                # Test si le sommets non-terminal n'a pas déjà subit un mouvement :
                if dico_internals[candidat_s] == False :
                    G_copy = G_init.copy() # copy profonde
                    G_copy.add_node(candidat_s, Internal=True)
                    for node1, node2 in G.edges() :
                        if node1==candidat_s or node2==candidat_s :
                            G_copy.add_edge(node1, node2, weight=G[node1][node2]['weight'])
                    if G_copy.degree(candidat_s) <= 1 :
                        continue
                    else :
                        G_test = nx.minimum_spanning_tree(G_copy)
                        G_test_fitness = calcul_fitness_arbre(G_test)
                        if G_test_fitness < fitness_solution :
                            voisin.append((G_test_fitness, G_test, candidat_s))
        
            # Boucle pour calculer les solutions avec la suppression d'un sommet non-terminaux de l'ensemble S :
            for candidat_s in nodes_S :
                # Test si le sommets non-terminal n'a pas déjà subit un mouvement :
                if dico_internals[candidat_s] == False :
                    G_copy = G_init.copy()
                    G_copy.remove_node(candidat_s)
                    # Test si le graph reste connexe :
                    if nx.is_connected(G_copy) == False :
                        continue
                    else :
                        G_test = nx.minimum_spanning_tree(G_copy)
                        G_test_fitness = calcul_fitness_arbre(G_test)
                        if G_test_fitness < fitness_solution :
                            voisin.append((G_test_fitness, G_test, candidat_s))
            
            # Test si il reste encore des mouvements :
            if len(voisin) == 0 :
                break
            
            # Fitness :
            fit, G_fit, candidat_s = min(voisin, key = lambda x: x[0])#voisin.pop(0)
            # Mise à jour des visites des sommets_non_terminaux :
            dico_internals[candidat_s] = True
                
        if fit < fitness_solution :
            fitness_solution = fit
            G_init = G_fit
        else  :
            break
        
    return fitness_solution
    
def test_recherche_locale(choix_heuristique) :
    for i in range(1,21) :
        if i < 10 :
            G = parse_instance("E/e0"+str(i)+".stp")
        else :
            G = parse_instance("E/e"+str(i)+".stp")
        start = time.time()
        resultat = recherche_locale(G, choix_heuristique)
        end = time.time()
        print("E/e0"+str(i)+".stp : " + str(resultat) + " time : " + str(end-start))
        
# Metaheuristiques pour la résolution du problème de l’arbre de Steiner de poids minimum


## Auteures

Etudiantes :

* Lachiheb 	Sarah
* Leroy      Cassandre

Sorbonne Université (UPMC) 2018

## Enoncer du projet

Le problème de l’arbre de Steiner de poids minimum est un problème d’optimisation combinatoire connu pour ses multiples applications dans le domaine de la conception de grands réseaux (télécommunication, distribution électrique, pipelines, ...), de circuits VLSI. Le problème de l’arbre de Steiner de poids minimum est NP-difficile. 

L’objet du projet est ici de tester quelques métaheuristiques pour ce problème, en particulier de comparer les résultats obtenus par un algorithme génétique et une méthode de recherche locale. 

## Rapport

Fichier  |
------------- | 
[Consigne](https://gitlab.com/ProjetLachiheb/metaheuristique_arbre_steiner/blob/master/Rapport/Consigne_Projet.pdf) |
[Rapport](https://gitlab.com/ProjetLachiheb/metaheuristique_arbre_steiner/blob/master/Rapport/Rapport.pdf) |


## Librairie de fonction 

Ensemble de fonction  |
------------- | 
[Fonctions](https://gitlab.com/ProjetLachiheb/metaheuristique_arbre_steiner/blob/master/Sources/Librairie_fonction.py) |
